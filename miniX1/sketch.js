    //variabel som skal bruges i forbindelse med baggrund
let bgColor;

function setup() {
    //put setup code here
  createCanvas(450, 400);
  print("hello world");
  frameRate(5);
}

function draw() {
    //Koden for baggrunden, som gør brug af variabler
bgColor = color(random(255), random(255), random(255));
  background(bgColor);

    //Tekst med ''Kittens''
textSize(32);
textStyle(BOLDITALIC);
text('KITTENS',150,90);

    //Tigerens øre
ellipseMode(RADIUS);
fill('yellow');
  ellipse(150,160,30,40);
ellipseMode(CENTER);
fill(100);
ellipse(150,160,30,45)

ellipseMode(RADIUS);
fill('yellow');
  ellipse(285,160,30,40);
ellipseMode(CENTER);
fill(100);
ellipse(285,160,30,45);

    //Tigerens hoved
  fill('yellow');
  beginShape();
  vertex(150,150);
  vertex(285,150);
  vertex(315,220);
  vertex(217.5,300);
  vertex(120,220);
  endShape(CLOSE);

    //Tigerens mund
noFill();
beginShape();
vertex(250,260);
bezierVertex(235,285,220,265,215,260);
endShape();

noFill();
beginShape();
vertex(180,260);
bezierVertex(195,285,220,265,215,260);
endShape();

    //Tigerens øjne
  fill('white');
  circle(265,207,50);
  circle(170,207,50);

  fill('black');
  circle(155,207,10);
  circle(275,195,10);

    //Tigerstriber
fill('black');
  triangle(170,150,185,150,177.5,175);
  triangle(200,150,235,150,217.5,190);
  triangle(250,150,265,150,257.5,175);

  // Tigerens snude
  fill('black');
beginShape();
vertex(200,230);
vertex(230,230);
vertex(240,240);
vertex(225,238);
vertex(217.5,255);
vertex(212.5,255);
vertex(205,238);
vertex(190,240);
endShape(CLOSE);

}
