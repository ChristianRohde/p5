[My Code](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX1/sketch.js)

[Link to Project](https://christianrohde.gitlab.io/p5/miniX1/)

## My First Readme

<img src ='MiniX1.png' width=600>

(line X.) = refers to the line in which the syntax is placed in sketch.js file

My intention for my first program was to see if i was able to draw a cat, by using simple tools from the Reference page of p5.js. I quickly realized that the colorscheme i chose, did not match my image of a small kitten, but instead looked more like a tiger, which is completely fine. The tiger is placed on a bagground, which changes colors 5 times a second.

<br>

My project consist of different kinds of syntaxes, for start i made a variable with the ''let'' function (line 2), which was necessary for me, in order to program a changing bagground. The variable, named bgColor, came to use under the 'function draw' section. It was set to be a mix of 3 colorvalues (line 13), that would generate a new color based on the numbers.
The background color, will the be set to the variable ''bgColor'' and the result is a wide arrange of changing colors. I did however adjust the framerate from the default value of 60, down to 5, as 60 colors a second became unbearable for the eye.

<br>

I then began to draw the shape of the tiger with the vertex 'beginShape()' code (line 38) and I used the exact same function for the nose (line 76). I was then able to fairly easily color the two shapes in yellow and black, with the use of the 'fill()' syntax (line 37 & 75). I created the eyes with two simple ellipse shapes (line 61-66) and the ears are a variation of the ellipse function, named ellipseMode()(line 22). The mouth is made with different kind of vertex() named bezierVertex(), which allowed me to curve the lines between points(line 50). The last thing i added to the project was a headline saying ''Kittens'' above the tiger. I made the letters with the text() syntax, which is located under Typography (line 19).

<br> 

I have learned a great deal from this first project. With no previous experience in programming, i ended up with a project im pretty satisfied with. I knew none of the functions, as described above, but was able, through trail and error, to create all sorts of shapes and colors and i got a small insight in the endless possibilities of variables. I became pretty frustraded during the first hours of work, when trying to make a symetric shape for the head and nose. These parts were quite tricky, as i wasnt able to navigate in where i wanted the connecting points to be, at least not as effecient as i wanted it to. I spend a while figureing out, how the values in the code, reflected in my sketch. I still havent figuered out the best method yet, however i am sure, that i should be able to create the shape of a tigerhead in less than 1.5 hours.

<br>

I expect to gain alot of inspiration from reading different peoples work, in terms of the use of function or ideas, which may be unknown to me or just the same elements as i have discovered, but managed differently. But I still highly believe, that i wouldnt be feeling this confidence in programming, had i only observed other people work and not programming on my own. 

<br>

I guess there is a quite a few similarities between coding and writing, for instance the use of characters. The alphabet and characters we use in punctuation is a key element in all programming languages - This enables us to communicate with the computer and form directions for the system to follow. A huge difference between reading and writing is the perception of the two in terms of its role as a skillset in modern society. Most people do not find the ability to code an essentiel part of life, but as a time changes and technology finds it way into every aspect of our lives, more and more people claim it to be a new form of literacy. The ability to code allow citizens to have a better understanding of the technology around them and therefore en greater chance to influence their lives. It has also become a usefull skillset to master, when it comes to employment, in multiple fields.

<br>

References:
https://p5js.org/reference/ 
