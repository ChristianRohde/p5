### MiniX3 - Throbber

[My Code](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX3/sketch.js)

[My Project!](https://christianrohde.gitlab.io/p5/miniX3/)

<br>

<img src ='miniX3_brain.png' width=600>
(I was in a bit of a hurry this week)

<br>

I wanted to further explore my brain sketch from my previous minix2 and spent some time wondering how i could incorporate this in my throbber. I wanted to express what the rainbow throbber on a mac, feels like to me - a non-cooperating pain in the ass. I tried to do exactly that by making an evil brain full of riddles.

<br>

For this sketch in used a For (loop) function in order to fill out the bagground with an ongoing generating bagground of '0' and '1'. Loop functions have a ton of potential on how to utilize them, but in terms of time, they work very differently compared to our perspective of the matter. If i programmed my sketch to only print zeroes, it wouldnt have apperad as if 20 new canvas' were printed on, every second (and it would have seem like the time had stopped and leaving the bagground frozen). However, because its ever changing, it visualizes exactly how i am able to generate infinite amounts of text (0 and 1's), with a simple loop. Espacially this aspect of the code being infinite is interesting, as mentioned in the text, by Hans Lammerant “How humans and machines negotiate experience of time'', computation time is not affected by natural time cycles as we are. They might not run infinitly as the hardware will give up at some point, but the software and the loops are in theory able to run forever. 
By changing the framerate of the program, we are also able to feel a form of power, in terms of manipulating what appears to be the computers time - making it run slower or faster.

<br>

I am personally not a big fan of the throbber. The reason being that it tells us nothing about how long we have to wait. It doesnt help at all, it only tells me that the computer is processing and doing that by illustrating the waiting time to be infinite (which it also feels like). While it may not be the best choice, in terms of computer health, you could probably save alot of your precious (limited) time on earth by restarting the computer if the throbber is dragging you through an ongoing punishment. The software dosent care about waisting your time, it got unlimited amount of it - you dont.

### References
Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)
