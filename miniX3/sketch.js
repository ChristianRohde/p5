let brainY = 0;
let hov = 1;
let angle = [72,144,216,288];
let matrix = ['0','1'];

//Object turned into a function
var qm = {
  x:100,
  y:20,
  display: function() {
    strokeWeight(5);
    stroke(255,255,255);
    fill(0);
    textSize(35);
    text('?',this.x,this.y);
  }
}


function setup(){
  createCanvas(windowWidth, windowHeight);
    frameRate(20);
    angleMode(DEGREES);

  }

function draw(){
  let num = 120;
  background(0,50);

//My shifting background
push()
  background(0)
    for(var i=0;i<windowWidth;i=i+25){
      for(var t=0;t<windowHeight;t=t+25){
        textSize(14);
        fill(8,240,11);
        text(random(matrix),i,t);
  }
}
pop()

//''?'' rotating
push();
translate(400,250);
  let conf = 360/num*(frameCount%num);
    rotate(conf);
      qm.display();
pop();

push();
  translate(400,250);
  let conf1 = 360/num*(frameCount%num);
    rotate(conf1+angle[0]);
      qm.display();
pop();

push();
  translate(400,250);
  let conf2 = 360/num*(frameCount%num);
    rotate(conf2+angle[1]);
      qm.display();
pop();

push();
  translate(400,250);
  let conf3 = 360/num*(frameCount%num);
    rotate(conf3+angle[2]);
      qm.display();
pop();

push();
  translate(400,250);
  let conf4 = 360/num*(frameCount%num);
    rotate(conf4+angle[3]);
      qm.display();
pop();


//Brain hovering up and down + sketch for brain
push();

if(brainY <= -20 || brainY >= 10){
  hov = hov * -1;
}
  brainY = brainY + hov

  translate(0,brainY);
  fill(252,157,207);
    strokeWeight(0);
    ellipse(370,250,75,95);
    ellipse(430,250,75,95);
    ellipse(350,275,50,50);
    ellipse(450,275,50,50);
    ellipse(455,260,50,50);
    ellipse(345,260,50,50);
    ellipse(355,235,50,50);
    ellipse(445,235,50,50);
    ellipse(385,230,30,50);
    ellipse(415,230,30,50);

  strokeWeight(3);
    stroke(255,78,109);
    bezier(410,240,435,240,390,220,450,210);
    bezier(418,235,425,243,445,239,440,265);
    bezier(437,296,432,285,451,287,452,281);
    bezier(437,296,432,285,421,292,429,278);
    bezier(470,240,461,240,465,254,454,250);
    bezier(470,240,461,240,465,254,470,255);
    line(400,220,400,278);
    bezier(355,225,370,225,378,235,380,242);
    bezier(331,241,344,249,362,247,370,230);
    bezier(400,278,385,270,370,270,360,276);
    bezier(377,272,370,264,375,257,380,255);

    noStroke();
    fill(255,255,255);
    ellipse(360,260,45,45);
    ellipse(440,260,45,45);


  //EYEBROWS
    strokeWeight(3);
    stroke('black');
    fill('black');
    triangle(340,230,385,250,340,220);
    triangle(460,230,415,250,460,220);

pop();

  //SHADOW
    strokeWeight(0);
    fill('black');
    ellipse(400,430,70,10);


}
