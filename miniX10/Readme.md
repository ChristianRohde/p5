#### Individual flowchart of my miniX7
<img src ='FLOWchart_minix10.png' width=600>

#### Group flowcharts
<img src ='Minix10Idea1.png' width=600>
<img src ='Minix10Idea2.jpg' width=600>

### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level? 
I guess the first thing that comes to my mind is that it is hard to forget or suppress my ‘’experience’’ as a programmer (even though I’ve only been coding for a few months). At my first semester at this Digital Design program, we did produce some flowcharts and I found the aspect of communicating the simplicity of algorithms back then, way easier, as compared to now. I think it is a result of me now knowing (to some degree) how the logic and building blocks of programming works. The idea that someone without programming experience should be able to understand the flowchart, forces me to communicate through an additional layer of abstraction (which is already present in my code, as it being built through an OOP approach) – as previously learned, this is not without concerns. That additional layer of abstraction allows for an idea to be communicated across multiple professions but sacrifices the details and nuances of how the code actually works. As for example, the bear hitting the trap is not technically triggered by the two colliding, but by the distance (between them) being smaller than a preset value.

<br>

### What are the technical challenges facing the two ideas and how are you going to address these? 
##### The personality/ability test:
I think the most challenging aspect of this program will be to maintain some sort of overview or making sure the structure is going to proceed as planned. The sketch will consist of multiple layers and directions to explore within the same sketch, so it is crucial with the sake of our sanity, that the sketch is well organized.
<br>

##### Feed it with your data:
At first glance I thought this would be the easier of the two ideas, but it might be more complicated than I thought. The program won’t feel alive or interesting, unless it feels responsive. The sketch has to have some degree of unpredictability, so it will need different data request and responses, and how this has to combine, so that your previous actions do not feel useless or without impact of the present state of the program.

### In which ways are the individual and the group flowcharts you produced useful? 
I feel like my first (individual) flowchart would have been more useful, if I made it before I actually began programming it. Then it very well could have helped me, thinking through the logic of my game. Me doing it afterwards, would still, of course, allow me to communicate the nature of the game with multiple people of different backgrounds, with no experience in coding -> and this is of course the strength of a flowchart. However it still feels like a chore as mentioned ‘’ Most programmers preferred not to bother with a flowchart or produced their flowcharts only after they were done writing code. ’’ (Ensmenger, pp.323) 

This is not to undermine the importance and usefulness of flowcharts, as I see as an important tool of communication, especially when it comes to more complex systems and algorithms (and I won’t call this game that complicated), Ensmenger writes that 

> ‘’ Flowcharts allow us to “see” soft- ware in ways that are otherwise impossible. Not only do they provide a visual record of the design of software systems (albeit, as we have seen, never an entirely accurate record), flowcharts can also serve as a map of the complex social, organizational, and technological relationships that comprise most large-scale software systems. ’’ (Ensmenger, pp.346)

I find this point to be extremely valid, as me thinking back to a previous presentation of just a small aspect of the streaming service, Spotify. During the analysis of spotify, by making a flowchart, my group and I were able to maintain an overview, which I do not think would have been possible otherwise - > this also led us down a path of analysis, that felt hidden for us beforehand.

<br>
I imagine that the group flowcharts will serve a much greater purpose to our project, and in fact it already has. It helped to collective visualize the whole concept and how each decision will impact the state of the program. This will hopefully result in a better collaboratively work experience, because we already established to path and direction of our sketch.


### Litterature:
- Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351

- Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226
