let start = 0
let inc = 0.007
let start1 = 0
let inc1 = 0.0078
let bearPosX = 0;
let bearI = [];
let countS = 0;
let luna;

//score
let life = 1;

let bY = 285;

//Timber
let min_timber = 3
let timber = []

//gravity
let jump = false; //by default, the bear is not jumping
let direction = 1; //how high the bear is jumping for each frame (Y axis)
let velocity = 3; //speed of jumping up
let jumpPower = 10;//how high the bear can jump
let fallingSpeed = 3;//speed of falling down
let minHeight = 285//the height of the groundlevel (earth)
let maxHeigth = 50;//the highest point the bear can reach
let jumpCounter = 0;//how many times the bear can jump in one 'jump'

//font
let font;


function preload(){
  //loading the bear pictures in a loop
     for(let i=0; i < 8; i++){
         bearI[i] = loadImage('bear-walk-'+ i +'.png');
     }
      beartrap = loadImage('beartrap.png');
      luna = loadImage('luna.png');
      //font = loadFont('ka1.ttf');
   }


function setup(){
  createCanvas(800,400);
  angleMode(DEGREES)
}

function draw(){
  background(13,0,50);
  image(luna,500,20,100,100)
    gravity();
    checkTimberNum();
    checkMis();
    hit();
    keyPressed();


//to create the mountains - check my mini 5 for a more detailed description
      stroke(73);
      let xoff = start;
      let xoff1 = start;
      let xoff2 = start1;
    for (let x = 0; x < width; x++){
        let y = noise(xoff)*height;
        let y1 = noise(xoff1+40)*height;
        let y2 = noise(xoff2+80)*height;
          line(x,y,x,400)

        push()
          stroke(100)
        line(x,y1,x,400)
        pop()

        push()
          stroke(127)
          line(x,y2,x,400);
        pop()
          xoff += inc;
          xoff1 += inc * 0.7
          xoff2 += inc1 * 0.4
      }
    start += inc*0.5;
    start1 += inc1*0.5

// ground
    push()
      strokeWeight(5);
      stroke(0, 112, 26)
      fill(112, 69, 0);
      rect(-5,335,810,70)
    pop()
  displayLife();
  checkLives();


  if (frameCount % 6 == 0){
    countS ++;
  }

image(bearI[countS%bearI.length],bearPosX,bY,70,50);

  for (let i = 0; i<timber.length; i++){
    timber[i].move();
    timber[i].show();
    }
}

//checking numbers - if under 3 -> create a new one
function checkTimberNum(){
  if (timber.length < min_timber){
    timber.push(new Timber());
  }
}


//checking traps reaching the left side, and deleting them
function checkMis(){
  for (let i = 0; i < timber.length; i++){
    if(timber[i].pos.x < 3){
      timber.splice(i,1);
    }
  }
}

//checking if the bear hits a trap
function hit(){
  for (let i = 0; i < timber.length; i++) {
    let d = int(dist(timber[i].pos.x,timber[i].pos.y,bearPosX,bY));
    if(d < 45){
      life--
    }
  }
}

function displayLife(){
  textSize(25);
  //textFont(font);
  fill(0)
  text('LIVES', 360, 360);
  text('REMAINING  '+ life,310,390)

}

//the whole code for how the gravity works
function gravity(){
  if(bY >= minHeight && jump == false){ //minHeight makes sure, we dont fall through the ground and the bear doesnt jump
    bY = bY;
    jumpCounter = 0//resets jumpcounter when landing
  } else{
  bY = bY + (direction*velocity); //this is what makes gravity work - the bY is the y value of the bear
}

if(jump == true){ //jump becomes true if the UP ARROW is pressed
  if(bY <= maxHeigth || jumpCounter >= jumpPower){ //if the bear reaches the highest point it can reach or if the bear tries to jump beyond the value of jumpCounter - it wont jump
    if(bY >= minHeight){
      bY = minHeight;
      //dont fall if the bear is at the minimum height
    }
    else{
      velocity = fallingSpeed;
    }
  } else{
      velocity = -jumpPower;//falling
      jumpCounter += 1 //add 1 to the jumpcounter
  }
}
  else{
    velocity = fallingSpeed;
  }

}

function checkLives(){
  if(life < 1){
    textSize(30);
    stroke(255)
    text('GAME OVER', 300,100)
    noLoop();
  }
}

function keyPressed() {
if (keyIsDown(RIGHT_ARROW)) {
bearPosX+=3;
} else if (keyIsDown(LEFT_ARROW)) {
bearPosX-=3;
}

if(keyCode === UP_ARROW){
  jump = true;
} else{
  jump = false
}

}

