let beartrap;

class Timber{
  constructor(){
    this.speed = floor(random(2,4));
    this.pos = new createVector(width+10,300);
    this.size = floor(random(40,60));
    this.timber_rotate = 30
  }
  move(){
    this.pos.x-=this.speed
  }
  show(){
    push()
    fill(105, 56, 0)
    translate(this.pos.x, this.pos.y);
    rotate(this.timber_rotate)
    image(beartrap, 0, 0-10, this.size, this.size)
    pop()
  }
}
