[My sketch](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX7/sketch.js)

[My object](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX7/bear.js)

[My Project](https://christianrohde.gitlab.io/p5/miniX7/)

(I haven't read it through - im too tired :) )

#### (DISCLAIMER)
My mind is already on vacation, so this may not be my must detailed or impressive work
- The font wont upload (for some reason - i did try to do it manually), so my text in in the runMe is not the intended one -> you can see the real one in the screenshot
- I should use a different background now, since if have reused the mountains 3 times now - so i wont go too deep into details on that part -> you can find it on my miniX5
- I wanted to create the bear as a class (object), but as mentioned, i am not mentally present right now (sunday night). I will definetley use the concept of classes way more from now on -> the bear was the first thing i created in this sketch, so i made it with my previous approach (pre-OOP).

<br>

<img src ='miniX7.png' width=600>

### The program
My program is heavily inspired by Winnie Soon's sketch for a [tofu game](https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction), that sketch was the basis of the first concept of my game. -> You might be able to tell, after reading my code structure

But now for the program, it is my tribute to the kids show 'Bear in the Big Blue House', where the user is able to control the bear with the arrowkeys and the moon Luna is displayed at the top. The objective is to avoid the bear traps and if you get caught in one of them, it will end the game, as you only got one life.
The bear traps are seperated from the sketch file, in a class structure. The js file is called bear, and the class is named Timber (the reason being, that i originally intended to use timber for obstacles). It contains different properties such as the size, which will be selected randomly between 40-66, how much they sould rotate when created, as well as how fast they should move and where they are able to move. 
The bear is created in the sketch.js and i did spent some time trying to figure out how to 'animate' the bear (disclaimer, i did not make it myself, it is a sprite from skratch.com) and by help from Daniel Shiffman, i made it work. It works with the help of a for(loop). It will run through each of the bear pictures (line 35) as a array and it is drawn at line 101. The bear animation requires an ongoing number, counting up from 0 to 7, therefore the array uses the modular operator, combined with the variable countS (used in line 97). The reason for the variable countS instead of just frameCount, is because the frameCount is way to fast, resulting in a less pleasing animation. 

(I explain the gravity and jumping mechanics in my sketch and i hope it makes sense.)
When the bear is to0 close to the center of a trap (being set to 50), it will subtract and when the life counter reaches below 1 it will end the game with a no loop, and a 'GAME OVER' text displayed on the canvas. 
When the bear avoids a trap and it reaches the end of the left side, the array of traps (which is set to a maximum of 3) - will splice itself, and the program will check, if it the maximum amount of traps are displayed -> if not, it will generate a new one.

<br>

### OOP
The Object Oriented Programming is an approach to programming, in which everything (or at least some aspects of it), is structured in a way, where objects are able to be classified and easily reused. This approach is very handy, when dealing with objects, with attributes, that goes beyond the most simple way of display, such as drawing a single ellipse at a specific place. If we deal more complex concepts of objects, the use of class to structure the program is usefull, but it requires a level of abstraction - meaning it is necessary for the program to be given a specific set of properties - that are important for the object. This point is present in Winnie Soon and Geoff Cox chapter of Object oriented programming 
        ''The understanding is that objects in the real world are highly complex and nonlinear, such abstracting and translating processes involve decision making to prioritize generalization while less attention is paid on differences.'' (p. 147) 

These properties are not determined by a neutral power, but by a programmer. This raises the question of bias and misrepresentation as related to our previous theme of Emojies and discrimination

<br>

Im not sure what cultural context my program might fit into, other than a tribute to the 'Bear in the Big Blue House', as a message to tell tv channels to please send some quality kids shows (in my humble opinion). Or it might be a statement to draw attention to the amount of bears being killed or kept hostage.(I really dont know at the moment)
The most obvious example of complex details and operations being abstracted, is as mentioned before, the complex idea of representation through emojies - how the deeper and richer the details might be, the more exclusive the emojies (might quickly) become.

<br>

#### References and Inspiration

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- https://www.youtube.com/watch?v=StoBCNiQakM (This was my guide on creating gravity)
- https://www.1001fonts.com/karmatic-arcade-font.html (The arcade game font)
- https://www.clipartmax.com/middle/m2i8m2G6b1H7A0i8_bear-trap-bear-trap/ (The beartrap photo)
- https://scratch.mit.edu/ (the source of the bear sprite)
- https://www.pinterest.dk/pin/777504323155077341/ (the source of the Luna png)
- https://www.youtube.com/watch?v=3noMeuufLZY (my guide on making the sprite animate)
