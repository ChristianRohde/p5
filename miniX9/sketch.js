// API key: RLkrJJBV946rfTox3AvfK3Ki8Ta09ITigGuBW2Or

// https://tle.ivanstanojevic.me/api/tle/

// https://tle.ivanstanojevic.me/api/tle/?api_key=RLkrJJBV946rfTox3AvfK3Ki8Ta09ITigGuBW2Or


let request;
let angle;
let satellite = [];
let globalData;
let satNum;
let satText;

let textY = 510;
let hov = 1;
let cirX = 100;
let clickedOn = 0;

let beam = [];
let countS = 0


function preload(){
  earthPic = loadImage('earth.png');
  bg = loadImage('bg.jpg');
  satImg = loadImage('satImg.png');

  for(let i=0; i < 2; i++){
      beam[i] = loadImage('beam'+ i +'.png');
  }
  customFont = loadFont('nasalization-rg.ttf');
}



//setup
function setup (){
request = 'https://tle.ivanstanojevic.me/api/tle/?api_key=RLkrJJBV946rfTox3AvfK3Ki8Ta09ITigGuBW2Or' //API Key
angle = 50

createCanvas (1080,720);

loadJSON(request, gotData);
angleMode(DEGREES);
textFont(customFont);

}

function gotData(data){
print(data);
globalData = data

}



//draw here
function draw() {


if(globalData){
  background (0);
  image(bg, 0, 0)


//earth
translate(0, 0);
push();
  rotate(angle);
  fill(0, 100, 200);
  image(earthPic, -300, -300);
pop();
//earth rotation
angle += 0.03
if (angle > 359){
  angle = 0
}

showText();

if(satellite.length < globalData.member.length){
  for(let j = 0; j < globalData.member.length; j++){
  satellite.push(new Satellite(floor(random(300,-1000)),floor(random(0, 720)),100, random(0, 360), j));
    for(let i = 0; i < satellite.length; i++){
      satNum = i

    }
  }
}


 for (let i = 0; i<satellite.length; i++){
    satellite[i].show();
    satellite[i].move();
    }

  }
}

function showText(){

push()
//translate(0,textY);
strokeWeight(3)
  textSize(40);
/*if(textY <= 500 || textY >= 530){
  hov = hov * -1;
}*/
if(textY <= 500){
  hov +=0.1;
} else if(textY >= 520){
  hov -=0.1;
}


textY = textY + hov

  textAlign(CENTER)
  blendMode(LIGHTEST);
  fill(27,197,255,60);
  text(satText,800,textY)
  fill(0,165,243,50);
  textSize(38)
  text(satText,800,textY);

  if(clickedOn === 0){

    fill(27,197,255);
    ellipse(800,610,90,10);
    //cirX = cirX + hov
    if (frameCount % 2 == 0){
      countS ++;
    }
    textSize(40)
    image(beam[countS%beam.length],715,450,160,160);
    textAlign(CENTER)
    blendMode(LIGHTEST);
    fill(27,197,255,60);
    text('CLICK ON',800,textY-50)
    text('A SATELLITE',800,textY-10)
    fill(0,165,243,50);
    textSize(38)
    text('CLICK ON',800,textY-50);
    text('A SATELLITE:',804,textY-10);
  }
pop()

if(clickedOn === 1){
  textAlign(CENTER)
  fill(27,197,255);
  ellipse(800,610,90,10);
  //cirX = cirX + hov
  textSize(40)
  fill(27,197,255,60);
  text('NASA NAME:',800,textY-40);
  textSize(38)
  fill(0,165,243,50);
  text('NASA NAME:',800,textY-40);
  if (frameCount % 2 == 0){
    countS ++;
  }
  image(beam[countS%beam.length],715,450,160,160);
}


}




function mousePressed(){
  for (let i = 0; i<satellite.length; i++){
     satellite[i].clicked(mouseX, mouseY);
     }
}
