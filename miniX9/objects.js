class Satellite{
  constructor(_x, _y, _size, _rotateStart, _sateNumber){
    this.size = _size + floor(random(-20, 20));
    this.x = _x;
    this.y = _y;
    this.rotate = _rotateStart;
    this.r = this.size/2
    this.sateNumber = _sateNumber
  }

  show(){
    fill(255);
    image(satImg, this.x,this.y,this.size, this.size)
  }

  place(){
    rotate(this.rotate)
  }

  move(){

    if(this.x < -this.size){
      this.y-=1
    }
    if(this.y < -this.size){
      this.x+=1
    }
    if(this.x > -this.size){
      this.y+=1
    }
    if(this.y > -this.size){
      this.x-=1
    }
  }

  clicked(px, py) {

      let d = dist(px, py, this.x, this.y);
      if (d < this.size+this.r) {
        clickedOn = 1
        satText = globalData.member[this.sateNumber].name

      }
    }

}
