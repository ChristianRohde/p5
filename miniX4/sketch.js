let button;
let mic;
let capture;
let ctracker;
let cry;
let vunC;
let countS = 0;
let barUP;
let brainY = 0;
let hov = 0.1;
let start = 0;
let img;



function preload(){
  img = loadImage('petpetter.png');

}

function setup() {
  createCanvas(640,480);
  background(100);
  //button
  button = createButton('Start Scanning');
  button.style("color", "#000000");
  button.style("background", "#2AFF01");

  button.position(width/2-50, height+10);
  button.mousePressed(change);
  button.mouseOut(revertStyle);


  // audio
  mic = new p5.AudioIn();
  mic.start();

  //video
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

}

function draw() {
 bar();
let volM = mic.getLevel();

if(start===1){
  let positions = ctracker.getCurrentPosition();
   if (positions.length) {
     for (let i = 0; i < positions.length; i++) {
if(countS===100){
     noStroke();
        ellipse(positions[27][0], positions[27][1], 60, 60);
        ellipse(positions[32][0], positions[32][1], 60, 60);

      push();
        let numeye = 60;
        translate(positions[27][0],positions[27][1])
        let eyeconfL = 360/numeye*(frameCount%numeye);
      rotate(eyeconfL)
        fill('black');
        ellipse(5,5,10,10);
      pop();
      push();
       let numeyeR = 60;
        translate(positions[32][0],positions[32][1])
        let eyeconfR = 360/numeye*(frameCount%numeyeR);
        rotate(-eyeconfR)
        fill('black');
        ellipse(10,10,10,10);
      pop();
      push();
      strokeWeight(10);
      stroke(0);
      noFill();
      bezier(positions[19][0], positions[19][1]-20,positions[20][0], positions[20][1]-10,positions[21][0], positions[21][1]-10,positions[22][0], positions[22][1]-10)
      bezier(positions[18][0], positions[18][1]-10,positions[17][0], positions[17][1]-10,positions[16][0], positions[16][1]-10,positions[15][0], positions[15][1]-20)
      pop();
    image(img,75,320,150,150);

    push();
    textFont('monospace');
    textSize(30);
    strokeWeight(5)
    stroke(random(255),random(255),random(255));
    fill(255);
    text('YOU NEED THIS',50,250);
    textSize(45);
    text('NOW',110,300);
    line(270,400,400,400);
    line(270,400,300,380);
    line(270,400,300,420);
    pop();

    push()
    stroke(0);
    fill(255);
    strokeWeight(1);
    textSize(15);
    text('GET 93% OFF', 330,350)
    text('WITH YOUR FIRST PURCHASE!',330,380)
    pop()


push()
    textFont('monospace');
    textSize(30);
    stroke(0,255,0);
    text('COMPLETE',250,90);
pop()


    }

if(countS<=99){
      push();
      noStroke()
      rectMode(CENTER);
      blendMode(LIGHTEST);
      fill(27,197,255,10);
      rect(positions[33][0],positions[33][1]+brainY,200,10,60);
      fill(0,165,243);
      rect(positions[33][0],positions[33][1]+brainY,200,1,60);
      pop();

      if(brainY <= -100 || brainY >= 150){
        hov = hov * -1;
      }
        brainY = brainY + hov
        push()
        fill(0,255,0);
        textFont('monospace');
        textSize(10);
        text('Analyzing Vulnerability ',230,20);
        pop()
     }

   }
 }
//text
push()
fill(0,255,0);
textFont('monospace');
textSize(30);
text(countS + '%', 280,50);
if (frameCount % 4 == 0){
  countS ++;
}
if (countS >=101){
  countS --;
}
pop();
}

}


function change() {
  button.style("background", "#21CC00");
  userStartAudio();
  if(start===0){
    start=1
  }

}

function revertStyle() {
  button.style("background", "#2AFF01");
}

//keycode..info
function keyPressed() {
  if (keyCode === 32 ) {
    button.style("transform", "rotate(170deg)");
  } else {
    button.style("transform", "rotate(10deg)");
  }
}

function bar(){
  let vol = mic.getLevel();
  let barX = map(vol,0,1,470,10);
    image(capture,0,0,640,480)

if(start===1 && countS<100){
    strokeWeight(3);
    stroke(0);
    rect(590,250,20,240,50);
  push();
    strokeWeight(10);
    stroke(0,255,0);
    line(600,480,600,barX);
  pop()

}



}
