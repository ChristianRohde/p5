[My Code](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX4/sketch.js)

[My Project](https://christianrohde.gitlab.io/p5/miniX4/)

<br>
<img src ='miniX4a.png' width=600>
<img src ='miniX4b.png' width=600>

## The Presence of Contemporary Service
'The Presence of Contemporary Service' is a project meant to visualize Shoshana Zuboff's comment about how corporations have created software systems, that are able to predict our mental state and thereby predict when it is the best time for an add to show up on our browser. This project is not a 'functional' project, meaning it can not analyze your expression but only give you one predetermined add.

<br>

#### The Code 
For this miniX i included a new set of librabries and for once made something interactive, with the use of a button and the ability to manipulate the angle of the button. The most dominant feature of my project has to be the facetracker. I used the facetracker along with the sound input, to create a rough sketch of how i would have vizualized the process of analyzing mental state (if i didnt had some knowledge on how these system actually works), in a fun hollywood fictional style. 
I did learn quite a lot on how were are able to utilize the web cam in our code, but also alot more of its shortages. The shortages including things like the amount of light necessary for it to work and the angle of your head etc. 
This miniX also confirmed me in the 'almost' endless possibilities of code and syntaxes, how we are able to work around the sound input ranging from 0-1 and using the map function, to scale those numbers up to 40-600.
This project took a bit more attention, than some of the previous miniXs and therefore i did not manage to create all the functions i wanted or a more aesthetically pleasing look, i normally would aim for.

### Data Capture
I would say that my work do relate to the theme of data capturing, as it hopefully speaks to the online user, that may not be aware of the amount of data they are producing and how adds are lurking and just waiting for the perfect moment to strike. Where firms wrap adds for new products as a friendly service, instead of an invasion of your privacy on the base of data, that you arent aware of they possess. You could also turn it around and look at my project as a critique of firms creating products, branded as special and customized, aimed directly to fulfill your needs - when in reality its really just a predetermined template. 
My first argument is based on and inspired by Shoshana Zuboff's documentary on Surveillance Capitalism and how firms like Google are able to create a extremely detailed description of you. Based on your activity on the internet and through disguised activities can control and hunt you like a pray, waiting for the perfect moment to attack, in order to profit.
This is problematic for a number of reasons. Ulises A. Mejias argue that this datafication in modern society functions as a new form of contemporary colonialism, as it approaches and invades human life with the goal of creating profit through a capitalistic system, without consideration of the ethical consequences. I do believe that the reason, many people do not consider the thread of this technology is, as mentioned, the lack of knowledge on the topic but most definitely also due to legalization. As this is still new territory, there havent been established laws to regulate firms use of personal data, as Zuboff mentions in her documentary, can be devastating, if used by the wrong people.

<br>

### References
Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.

Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. 

