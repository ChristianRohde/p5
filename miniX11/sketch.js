// Global Variables start ______________________________________________________
let start = 0; //used to check whether the game has been started or not.
let enterName// the button clicked when giving the blob a name
let blobName; //the value of the blob name
let selection = 0; //used by cursor to check which button it is highlighting
let mood = 0; //each mood is assigned a number. This changes accordingly
let health = 3; //how many lives does the blob has
let poop = [];
let blobLevel = 1;
let fed = false;

let timeReset = 0;
let totalTime = 0;
let bubbleTimer = 0;
let foodTimer = 0;
let cryTimer = 0;
let poopTimer = 0;
let petTimer = 0;
let noTimer = 0;
let animationTimer = 0;
let animationStart = 0;
let hungerTimeout = 30; //amount of time for you to feed the blob (changing will mess with size of bar)
let timeBetweenMeals = 16; //how many seconds until blob becomes hungry
let timeBetweenPoops = 6; //how many seconds until blob poops

let amountPet = 0;
let amountFeed = 0;
let amountClean = 0;

//related  to data-caption
let enter, input, cancel, please, seeData; //name of buttons
let startAskData = false;
let data = []; // array where the input is stored
let dataQuestions; //JSON file with questions
let dataRights = false;
let json;
let n = 0;
// Global Variables end ________________________________________________________



// Preload start _______________________________________________________________
function preload(){
  ballGif = loadImage('gifs/blob_ball.gif');
  idleGif = loadImage('gifs/blob_idle1.gif');
  idle2Gif = loadImage('gifs/blob_idle2.gif');
  idle3Gif = loadImage('gifs/blob_idle3.gif');
  hungryGif = loadImage('gifs/blob_hungry1.gif');
  satisfiedGif = loadImage('gifs/blob_love1.gif');
  cryGif = loadImage('gifs/blob_cry1.gif');
  hungryIconGif = loadImage('gifs/Apple.gif');
  deadGif = loadImage('gifs/blob_dead1.gif');
  petGif = loadImage('gifs/blob_happy1.gif');
  jumpGif = loadImage('gifs/blob_jump1.gif');
  noGif = loadImage('gifs/blob_no.gif')
  rainbowGif = loadImage('gifs/rainbow.gif');
  skullGif = loadImage('gifs/skull.gif');
  foodBowlGif = loadImage('gifs/bowl.gif');

  bgImg = loadImage('img/back.png');
  introimg = loadImage('img/titlecard.png');
  shadowImg = loadImage('img/shadow.png');
  speechImg = loadImage('img/Talkbox.png');
  poopImg = loadImage('img/blob_poop1.png');
  heartFullImg = loadImage('img/heart_full.png');
  heartEmptyImg = loadImage('img/heart_empty.png');
  panic1Img = loadImage('img/panic_1.png');
  panic2Img = loadImage('img/panic_2.png');

  customFont = loadFont('font/upheavtt.ttf');

  dataQuestions = loadJSON('dataQuestions.json');
}
// Preload end _________________________________________________________________



// Setup start__________________________________________________________________
function setup(){

  textFont(customFont);
  createCanvas (900, 700);
  //__\\
  blob = new Blob(300, 130, 300, 300);
  //__\\
  textCursor = new TextCursor(220, 602);
  //__\\
  speBub = new SpeechBubble(400, 80, 250);
  icon = new Icon(510, 84, 150)
  //__\\
  petBut = new Word(220, 616, "PET");
  feedBut = new Word(440, 616, "FEED");
  cleanBut = new Word(660, 616, "CLEAN");
  //__\\
  topLevel = new Word(640, 57, "Lv. 1");
  topLevel2 = new Word(640, 57, "Lv. 2");
  topLevel3 = new Word(640, 57, "Lv. 3");
  topLevel4 = new Word(640, 57, "Lv. 4");
  //__\\

  //dom elements related to data caption
  enterName = select('#enterName');
  enterName.center();
  enterName.position(700,600);
  enterName.size(100,47)
  enterName.mouseClicked(nameBlob);

  /*
  nameMsg = createP('What is the name of your friend?');
  nameMsg.style('font-size','32px');
  nameMsg.style('font-family', customFont);
  nameMsg.style('color','white');
  nameMsg.position(100,515);
  */

  enter = select('#enter');
  enter.center();
  enter.position(650,600);
  enter.size(100,47)
  enter.mouseClicked(storeData);
  enter.hide()

  cancel = select('#cancel');
  cancel.center();
  cancel.position(755,600);
  cancel.size(110,47)
  cancel.mouseClicked(letStarve);
  cancel.hide()

  input = select('#answer');
  input.center()
  input.position(90,600);
  input.size(550,40);

  please = createP('Please be more specific');
  please.hide();

  seeData = select('#seeData');
  seeData.center();
  seeData.position(404,324);
  seeData.size(80,40);
  seeData.mouseClicked(createJSON);
  seeData.hide();

}
// Setup end ___________________________________________________________________



// Draw start___________________________________________________________________
function draw(){
  drawBackground();
  firstAnimation();
  moodCheck();
  timerCheck();
  healthCheck();
  levelUp();
  askFood();


  if (animationStart === 0) {
    blob.showBall();
  }


if (start === 1) {

  generatePoop();
  levelCheck();

  textCursor.show();
  textCursor.check();

  topName.showName();


  petBut.show();
  feedBut.show();
  cleanBut.show();
}


  if(startAskData){
    askData();
  }
  if(dataRights){ //if data = true
    displayData();
  }

}
// Draw end ____________________________________________________________________



// drawBackground start ________________________________________________________
function drawBackground(){
image(bgImg, 0, 0)
image(shadowImg, 295, 250, 300, 200)
push();
  stroke(230);
  strokeWeight(5);
  fill(111, 127, 159);
  rect(10, 10, 880, 60);
  rect(10, 510, 880, 180);
pop();

}
// drawBackground end __________________________________________________________



// nameBlob start ______________________________________________________________
function nameBlob(){
  //start = 1;
  animationStart = 1
  blobName = input.value();
  topName = new Word(15, 57, blobName);
  input.value("");
  enterName.hide();
  input.hide();
  //nameMsg.hide();

}
// nameBlob end ________________________________________________________________



// moodCheck start _____________________________________________________________
function moodCheck() {
if(start === 1){
  if (mood === 0 && blobLevel === 1 || mood === 0 && blobLevel === 2){
    blob.idle();
  } else if (mood === 0 && blobLevel === 3){
    blob.idle2();
  } else if (mood === 0 && blobLevel === 4){
    blob.idle3();
  } else if (mood === 1){
    blob.hungry();
  } else if (mood === 2 && fed === true){
    blob.satisfiedFood();
  } else if (mood === 2){
    blob.satisfied();
  } else if (mood === 3){
    blob.cryAlot();
  } else if (mood === 4){
    blob.cryAlittle();
  } else if (mood === 5){
    blob.doNotWant();
  } else if (mood === 6){
    blob.pet();
  } else if (mood === 7){
    blob.die();
    seeData.show();
    }
  }
}
// moodCheck end _______________________________________________________________



// healthCheck start ___________________________________________________________
function healthCheck(){
if(start === 1) {
  if (health === 3){
    blob.healthGreat();
  } else if (health === 2){
    blob.healthGood();
  } else if (health === 1){
    blob.healthBad();
  } else if (health === 0){
    blob.healthDead();
    mood = 7 //dead
    }
  }
}



//levelCheck start _____________________________________________________________
function levelCheck(){
  if(blobLevel === 1) {
      topLevel.show();

    } else if (blobLevel === 2){
      topLevel2.show();

    } else if (blobLevel === 3){
      topLevel3.show();

    } else if (blobLevel === 4){
      topLevel4.show();
  }
}



// timerCheck start ____________________________________________________________
// Various timers for different situations, allowing for individual changes without potentiel complications
function timerCheck(){

  if (frameCount % 60 == 0){
    totalTime ++;
  }

  if (frameCount % 60 == 0){
    foodTimer ++;
  }

  if (frameCount % 60 == 0){
    bubbleTimer ++;
  }

  if (frameCount % 60 == 0){
    cryTimer ++;
  }

  if (frameCount % 60 == 0){
    poopTimer ++;
  }

  if (frameCount % 60 == 0){
    petTimer ++;
  }

  if (frameCount % 60 == 0){
    noTimer ++;
  }

  if (animationStart === 1){
    if (frameCount % 60 == 0){
      animationTimer ++;
    }
  }


  if (mood === 7) { //forces timers to stop, so the mood doesn't change anymore
    poop =[];
    resetTimers();
  }


  //for debugging, ignore --->
  /*
  text("food " + foodTimer, 200, 200)
  text("bubble " + bubbleTimer, 200, 220)
  text("cry " + cryTimer, 200, 240)
  text("pet " + petTimer, 200, 260)
  text("poop " + poopTimer, 200, 280)

  text("pet " + amountPet, 100, 200)
  text("feed " + amountFeed, 100, 220)
  text("clean " + amountClean, 100, 240)
  <---- */
}

// timerCheck end ______________________________________________________________



// firstAnimation start ________________________________________________________
function firstAnimation(){
if (animationStart === 1){
  blob.playJump();
  if (animationTimer > 2) {
    animationStart = 0;
    start = 1
    resetTimers();
    }
  }
}
// firstAnimation end __________________________________________________________



// askFood start _______________________________________________________________
function askFood() {
  if (start === 1){
    if (foodTimer > timeBetweenMeals) {
      mood = 1

      speBub.show();
      icon.showHungry();

      timerBar = new FoodTimerBar(763, 450, hungerTimeout);
        if (hungerTimeout > 0) {
          timerBar.show();

          if (frameCount % 60 == 0){
            hungerTimeout --;
    }
            if (hungerTimeout === 0) {
              health--
              resetTimers();
              mood = 4
              please.hide();
              enter.hide();
              cancel.hide();
              input.hide();
              startAskData = false;

        }
      }
    }
  }
}
// askFood end _________________________________________________________________



// letStarve start_______________________________________________________________
function letStarve(){
  health--;
  resetTimers();
  mood = 4;
  please.hide();
  enter.hide();
  cancel.hide();
  input.hide();
  startAskData = false;
}
// letStarve end_______________________________________________________________



// generatePoop start __________________________________________________________
function generatePoop (){
  if(mood === 0 || mood === 3){
    if (poopTimer === timeBetweenPoops){
      poopTimer = 0;
      poop.push(new Poop(random(0, 830), random(70, 420), random(100, 150)))
    }
  }

  if (poopTimer > timeBetweenPoops){
    poopTimer = 0;
  }

  for(let i = 0; i < poop.length; i++){
    poop[i].show();
  }

  if(poop.length > 3){ //if there are X amount of poop on screen then it changes to constant crying
    if(mood === 1 || mood === 7){
      } else {
        mood = 3
    }
  }
}
// generatePoop end ____________________________________________________________



// levelUp start _______________________________________________________________
function levelUp(){
  if(amountFeed === 4){
    blobLevel = 2
  } else if(amountFeed === 8){
    blobLevel = 3
  } else if(amountFeed === 12){
    blobLevel = 4
  }
}
// levelUp end _________________________________________________________________



// resetTimers start ___________________________________________________________
function resetTimers(){
  foodTimer = 0;
  cryTimer = 0;
  bubbleTimer = 0;
  petTimer = 0;
  poopTimer = 0;
  hungerTimeout = 30;
}
// resetTimers end _____________________________________________________________



// keyPressed start ____________________________________________________________
function keyPressed() {
  if (start === 1) {
    if (keyCode === RIGHT_ARROW) {
      textCursor.dir(220, 0);
      textCursor.move();
    } else if (keyCode === LEFT_ARROW) {
      textCursor.dir(-220, 0);
      textCursor.move();
    }


  if (keyCode === 32){

    if (selection === 1){
      if (mood === 0){
        amountPet++
        petTimer = 0;
        mood = 6;
      }

  } else if (selection === 2){
      bubbleTimer = 0;
        if (mood === 1 && amountFeed < 13){
          startAskData = true;
      } else {
        noTimer = 0;
        mood = 5;
      }

  } else if (selection === 3) {
      if (mood === 0 || mood === 3){
        fed = false
        amountClean++
        poop = []
        bubbleTimer = 0
        mood = 2

        }
      }
    }
  }
}
// keyPressed end ______________________________________________________________
