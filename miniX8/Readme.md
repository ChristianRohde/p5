
[My Code](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX8/sketch.js)


[My Class](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX8/politicalParties.js)



[My Project](https://christianrohde.gitlab.io/p5/miniX8/)


<img src ='miniX8.png' width=600>

### MiniX8 (Svend & Christian)

##### C0NT3MP0RARY P0LAR1ZAT10N

In the digital age political polarization has grown, especially in the US, due to social media’s algorithms prioritizing clicks and reinforcing beliefs rather than political enlightenment and expanding the users horizon. This has let to a political environment undermining democracy, as voters and politicians do not operate on the same datas and facts. In our work C0NT3MP0RARY P0LAR1ZAT10N this is illustrated by a text-message program where the Republican and Democrat party are sending each other messages in an incoherent political debate. Note that these opposing political facts and statements are not loaded from the same database, but from separated JSON-files containing facebook comments from actual facebook users. What is at stake is expressed in the vocable source code proclaiming that our factual basis must be equal for democracy to thrive (sketch.js line 39).  If they are not operating on the same datas and facts how are they supposed to understand and listen to each other? 



### How our program works and what syntax we used, and learnt? 

For this miniX we wanted to create a generative chatbox with two ‘’users’’, who by the first glance seem to communicate, but by closer look the viewer will notice that their messages are not corresponding to each other. We had the goal of creating two separate JSON files, representing the right and left side of the chat to enforce the idea of a lack of common ground. The JSON files contain multiple quotes, collected from President Joe Bidens, as well as Donald J. Trump’s Facebook page. The quotes are structured inside an array of objects, named ‘’posted’’.
 
Throughout our process, we had some difficulties getting the shapes to work in the object-oriented manner we wanted them to. At first we intended to use CSS styling, in order to capture the look of a Facebook chat, however, we did not manage to move our DOM elements in p5 js, without dragging a trail throughout the canvas. We changed tacit and replaced the DOM button, with a more simple build chatbox, constructed with p5 js 2D shapes. It made our classes way easier to work with in our sketch.js, but also left us with new problems. The createbutton() function are able to adjust the length as well as compress
the height of the box, when it comes too close to the edge of the canvas.
To compensate for this, we made use of the textBounds() function, which allows a shape (such as rect() ) to adjust the object according to the length of a given text string. We still needed to make sure that our chat messages were line breaking, so it would fit the parameters. This was easily done by manually dividing our quotes inside the JSON files, with ‘’\n’’ and tweaking a few values inside of our class sketch (politicalParties.js).
As this was our first coding project, collaborating as a group, we were quite vocal about our expectations and work strategy before diving into the coding. We agreed on the common goal of building the fundamentals, in terms of functionality, before focusing on the poetic value inside our code, which we later addressed. The open dialogue made for a really enjoyable collaborative experience, providing us with the opportunity to work on separate aspects of our sketch, with the fear excluding the other in determining decisions.
We began the coding process by working collectively in the same sketch, using the atom extension ‘teletype’. Even Though teletype did allow for us to work simultaneously, the workflow was quite disrupted by various bugs. We therefore decided to meet up in person, which made our collaboration way easier and efficient.

We chose to work with the Facebook styling, as Facebook is the largest social media platform with a staggering number of over 2 billion active accounts, each month.  (https://buffer.com/library/social-media-sites/) And because of our observation of facebook as a common battleground for debating - especially politics, where the 2020 US presidential election candidates are no exception.

### Analyzation of our work:

The work C0NT3MP0RARY P0LAR1ZAT10N is a javascript based program expressing the political polarization in the US. The work can be explained as vocable code as it deals with code’s poetic abilities. This can be seen in this snippet: 
<br>


“_text(republican.posted[this.political].statement (...)_” (politicalParties.js line 40). While keeping the functionality of telling the program which string to post the code also bears some textuality. 
This follows neatly along the following quote from Soon & Cox:
‘’_Code is both script and performance, and in this sense is always ready to do something: it says what it will do, and does it at the same time._‘’ - AP p. 167

<br>

The snippet of code signifies a command and a specific path for the computer to locate the data, while also signifying a meaning, which only the human can denote. This two-fold signification is possible due to the use of variables. Derek Robinson describes the variable as: “_(...) an empty signifier that awaits only assignment to contain a content._” p. 261 Fuller, Software Studies: A Lexicon. Because of the variable being an empty signifier it can denote one thing the computer, and another to the human.

<br>

Furthermore the work explores temporality. The textual meaning unfolds over time as the messages are sent with an interval of 300 frames (or 5 seconds), to mimic that the sender is human and thus needs time to type the message. 
This follows Lammarants notion that “_the experience of time is a socially negotiated and technically implemented experience._” Lammarant, How Humans and Machines Negotiate Time p. 97-98. Even though the data is preloaded and could technically be shown at once, the program gradually unfolds the data set over time to mimic the temporal experience of a political discussion. 

<br>

The work deals with a perception of a divided political landscape, which is strongly affected by what seems to be a lack of communication. This is made clear throughout the text strings, which will show in random order. That random function results in a unique conversation, everytime the program is executed, as it will leave no room for predicting how the debate will unfold.



#### Reflection on our work in terms of Vocable Code

Throughout the whole process we were sure that we wanted to make the code executable. After realizing this we discussed where we stood in the discussion of text as code and text as code (Caley/Marino) and found that while we agree that unexecutable code can be understood as code, we believe that the functional aspect of code is essential to the performance of vocable code. This was also reflected in our work process as we prioritized working on the functional aspect first and later renamed the variables to signify poetic qualities.

#### Literature:
- Lua, Alfred. (). 21 Top Social Media Sites to Consider for Your Brand. Located - 10. of april 2021 on: https://buffer.com/library/social-media-sites/
- Cayley, John. "The Code is not the Text (Unless it is the Text)." Electronic Book Review, 2002. http://electronicbookreview.com/essay/the-code-is-not-the-text-unless-it-is-the-text.
- Lammerant, Hans. “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190.
- Robinson, Derek. "Variable." Software Studies: A Lexicon. Ed. Matthew Fuller. The MIT Press, 2008. 260-266.
- Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186

#### Other References and inspiration:
- https://fontlibrary.org/en/font/prodigy-sans
- https://creative-coding.decontextualize.com/text-and-type/
- https://p5js.org/reference/#/p5.Font/textBounds
- Fan, Lai-Tze & Montfort, Nick. Dial (2020). http://thenewriver.us/dial/ 
- Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.
Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017),https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r
https://jsonformatter.curiousconcept.com
https://www.zedge.net/find/ringtones/facebook%20chat%20sound 
