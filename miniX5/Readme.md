[My Code](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX5/sketch.js)

[My Project](https://christianrohde.gitlab.io/p5/miniX5/)

<img src ='miniX5.png' width=600>

### My Rules (I know these really arent rules... )
#### 1. Draw lines across the canvas with different height values
#### 2. Continuously move the x coordinates of each line, so new lines can appear

________________________________________________________________________________________________________________________________________

##### Thoughts behind my code
I had a vision of creating a generative landscape, with the single goal of having a mountain develop itself throughout the program. Through my own trail and error, i manged to connect a set of ''random'' points with the noise() and bezier() functions. However, i did struggle quiet a bit with having my project coming to life and letting it evolve over time. 
I turned to a Daniel Shiffman video, in order to learn more about the noise() function in p5, but quickly found out that he had created a sketch in one of his videos, with the concept i was chasing. 

The code is not particular complicated, but i do like the outcome. The three chains of mountains, is made with the same concept, but manipulating different values, such as changing how closely each point is connected to the one next to it. I also added a value to shift the base noise() values, to different points for each mountain, so it would appear the mountains arent based on the same Perlin noised fixed numbers (see line 55) and having them move in different speed (see line 63+64). 
The emergent behvavior unfolds as a product of each line being so closely placed, that it creates the basic body of the mountain combined with the color value which is connected to the value of the moon's y coordinate. This results in 'dynamic' mountain ranges, stacked in layers, giving the sketch some depth and allowing it to develop over time.

#### The Role of Rules
The predetermined rules is the backbone of my entire sketch, without it the POV of the user would not be able to shift and the organic development of the mountains would have been close to impossible to recreate with my own values. This whole concept is heavily inspired by Ken Perlin and his invention of Perlin Noise and how this concept is able to produce what feels to be, way more realistic textures in order to produce terrain. (Montfort et al. 2012 p.139) This notion of controleable 'randomness' is extremely fascinating to me and how we as programmers are able to utilize it, so we can have something as mechanic as a computer, create dynamic and organic shapes form an illusion of life. I was able to control the entire frame of the sketch, and even tho the mountains seems to be drawn on random places, it doesnt take long to realise that the randomness is not that random at all. The parameters i chose for whole code, restricts the randomness of the noise() function to a degree, where it doesnt affect our perception of natural landscape and the possibilities of it. Had the values been too random, it could lead to a point where a user wouldnt be able to tell exactly that the sketch is meant to portray. - it restrains the randomness to a point where my vision (as a designer) of the project is less likely to be misinterpreted by a viewer. (This definetly also a huge constraint)

This last comment (The one above) and my thoughtprocess behind it, gave some me an additional perception of who exactly the artist is in a sketch such as this. Had i not constrained my sketch to this point and allowed the program to unfold itself, then it could have been generating art with 'endless' possibilites and a sense of 'freedom'(Which definetly isnt freedom at all, as it is told exactly what to do, but anyways) to the computer -> and does this affect the relationship between the programmer and computer to a degree where the credits of the generative art should be more towards the computer, than the human programmer?


#### References and Inspiration 

https://www.youtube.com/watch?v=Qf4dIN99e2w
https://www.youtube.com/watch?v=YcdldZ1E9gU
https://www.youtube.com/watch?v=y7sgcFhk6ZM 

https://www.google.com/search?q=adventure+time+ice+kingdom&source=lnms&tbm=isch&biw=1146&bih=648 

Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146. 
