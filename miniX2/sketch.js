let bgFade=0;
let angle=1;
let angle1=72;
let angle2=144;
let angle3=216;
let angle4=288;
let wordS=20;


function setup() {
  createCanvas(1200,600);
  frameRate(60);
  angleMode(DEGREES);

}

function draw(){
  //print(mouseX,mouseY);
  //console.log(bgFade);
  //console.log(wordS);


//RIGHTSIDE - bg ('if' syntax - making sure that the background resets)
background(55,55,55,bgFade);
  bgFade+=0.01;
  if (bgFade>5){
    clear();
    bgFade=0;
}

//TEXT GROWING AND RESETTING
wordS+=0.06;
if (wordS>50){
  wordS=20
}

//TEXT AROUND THE BRAIN
push()
  translate(900,250);
  rotate(angle);
  textSize(wordS);
  fill(150,150,150);
  stroke(0,0,0)
  text('PANIC',100,0);
  angle=angle+1;
pop()

push()
translate(900,250);
rotate(angle1);
textSize(wordS);
fill(150,150,150);
stroke(0,0,0)
text('PANIC',100,0);
angle1=angle1+1
pop()

push()
translate(900,250);
rotate(angle2);
textSize(wordS);
fill(150,150,150);
stroke(0,0,0)
text('PANIC',100,0);
angle2=angle2+1
pop()

push()
translate(900,250);
rotate(angle3);
textSize(wordS);
fill(150,150,150);
stroke(0,0,0)
text('PANIC',100,0);
angle3=angle3+1
pop()

push()
translate(900,250);
rotate(angle4);
textSize(wordS);
fill(150,150,150);
stroke(0,0,0)
text('PANIC',100,0);
angle4=angle4+1
pop()

//BRAIN SHAPE (RIGHT)
fill(252,157,207);
  strokeWeight(0);
  ellipse(870,250,75,95);
  ellipse(930,250,75,95);
  ellipse(850,275,50,50);
  ellipse(950,275,50,50);
  ellipse(955,260,50,50);
  ellipse(845,260,50,50);
  ellipse(855,235,50,50);
  ellipse(945,235,50,50);
  ellipse(885,230,30,50);
  ellipse(915,230,30,50);

strokeWeight(3);
  stroke(255,78,109);
  bezier(910,240,935,240,890,220,950,210);
  bezier(918,235,925,243,945,239,940,265);
  bezier(937,296,932,285,951,287,952,281);
  bezier(937,296,932,285,921,292,929,278);
  bezier(970,240,961,240,965,254,954,250);
  bezier(970,240,961,240,965,254,970,255);
  line(900,220,900,278);
  bezier(855,225,870,225,878,235,880,242);
  bezier(831,241,844,249,862,247,870,230);
  bezier(900,278,885,270,870,270,860,276);
  bezier(877,272,870,264,875,257,880,255);




//Venstre side - bg
fill(140,140,140);
stroke('black');
rect(0, 0, 600, 600);

//LEFT SIDE - TRANSLATE TO MOVE THE POINT OF CENTER
push();
translate(-600,0);
//BRAIN SHAPE (LEFT)
fill(252,157,207);
  strokeWeight(0);
  ellipse(870,250,75,95);
  ellipse(930,250,75,95);
  ellipse(850,275,50,50);
  ellipse(950,275,50,50);
  ellipse(955,260,50,50);
  ellipse(845,260,50,50);
  ellipse(855,235,50,50);
  ellipse(945,235,50,50);
  ellipse(885,230,30,50);
  ellipse(915,230,30,50);

strokeWeight(3);
  stroke(255,78,109);
  bezier(910,240,935,240,890,220,950,210);
  bezier(918,235,925,243,945,239,940,265);
  bezier(937,296,932,285,951,287,952,281);
  bezier(937,296,932,285,921,292,929,278);
  bezier(970,240,961,240,965,254,954,250);
  bezier(970,240,961,240,965,254,970,255);
  line(900,220,900,278);
  bezier(855,225,870,225,878,235,880,242);
  bezier(831,241,844,249,862,247,870,230);
  bezier(900,278,885,270,870,270,860,276);
  bezier(877,272,870,264,875,257,880,255);


//BRAIN FACE
  //Eyes
  noStroke();
  fill(255,255,255);
  ellipse(860,260,45,45);
  ellipse(940,260,45,45);

//EYEBROWS
  strokeWeight(3);
  stroke('black');
  fill('black');
  triangle(840,230,885,250,840,220);
  triangle(960,230,915,250,960,220);

//MOUTH
  strokeWeight(4);
  noFill()
  bezier(880,290,890,280,910,280,920,290);

//SHADOW
  strokeWeight(0);
  fill('black');
  ellipse(900,330,70,10)

pop();


}
