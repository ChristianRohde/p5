[My Code](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX2/sketch.js)

[Check out my project!](https://christianrohde.gitlab.io/p5/miniX2/)

### MiniX2
<br>

<img src ='MiniX2.png' width=600>


So for my second project i have made 2 'emojies'. On the left side there is placed a hovering brain in the center of the picture on a grey background, with an angry facial expression and a shadow beneath it. On the right side i used the same brain, but instead of giving it a face, i wrote 5 texts saying ''Panic'' circling around it. background on the left side start by being white, but slowly fading into a dark greyish color. The 5xPanic is set to increase in size in relation to the fading background and then resetting when a certain value is achieved. My goal was to express my feeling of anxiety, where the fading is to symbolize my body shutting down, while my thoughts are growing larger and larger resulting in trapping my mind. (The left one is just a simple angry brain :) )

<br>

The task required me to make 2 emojies using/exploring geometry and related syntaxes. I began by making the shape of the brains with a combination of different size ellipses and setting their strokeWeight() to 0, so that it would form an even color without any lines stacking up on eachother (line 88-100). I then gave it details by using bezier() that allowed me to create curved lines and by setting its strokeWeight() to 3, it ended up looking like a simple cartoonish brain (atleast i think so). 

<br>

(Disclaimer - I do realize that it would take me 10 minuttes instead of 1.5 hour, to just paint the brain in a sketching program at inserting that image... But then i wouldnt have explored different geometry)

<br>

After the first brain i (as you can see in my project) wanted to copy the image and place it on the left side, but after spending to much time trying to find the right coordinates for the first one, i wasnt planing to repeat the process once again. A way of avoiding this was with the use of translate() and with this syntax, i moved the point of (0,0) to (-600,0). By doing this i was able to just copy and paste the brain-code to running it once again. This however would have left me with two brains stacking upon eachother, so inorder to isolate the second brain (on the left side) i utilized the push() and pop() syntax.

<br>

I could fairly easily write the first ''Panic'' text on the left side of the brain, but i wanted it to circle around it. The rotate() function would allow me to make it rotate, but not around the brain as it was not the center. I once again used push() and pop() and moved the center of the canvas to the center of the brain (with translate()). The text needed to increase in size, to a certain point, and to do that i made a variable and an if-statement(), so if the value of the variable (called wordS) reached '50' it would reset to '20'. The same goes for the fading bagground (but made with its own variables).

<br>

By making this second project i felt more comfortable with how the atom/p5.js worked. This allowed me to feel a certain need to include changing aspects of the final sketch, instead of just static and while it was fulfilling to make things move, it also came with alot of frustration (when it wasn't working).

<br>

## Emojies beyond circles and squares

<br>

The whole use of emojies have troubled me for quiet a while, or at least since it evolved frem being the once ''classic'' yellow circle with two eyes and a mouth. I find find it troubleing as when the era of emojies (as the one mentioned in ''Modifying the Universal'', by Unicode Consortium) found its way into our lives, it brought alot of problematic aspects with it aswell as Abbing, Pierrot and Snelting speaks of, as the implementation of the 'skin tone modifer' would force a user into a restricted emoji of multiple aspects, which could very quickly result in a hurtful steriotype (page 42). I do acknowledge this problematic state of emoji culture and im not sure i agree that a more detailed and larger range of choices (in terms of emojies) will result in more people feeling represented. Im afraid it might do the opposite, as the amount of details these companies are trying to include will only leave a huge amount of people, feeling misrepresented. The idea of this responsibility scares me (quite alot), as i do not believe i am in a position where i feel informed enough to help produce a better solution. 

<br>

And because of this i wanted to express emotions (through the emoji), while not creating a human character but instead a brain, which i hope people find very universal. The left brain was my take on a angry or dissatisfied emotion, while the right is me trying to capture what an anxiety attack feels like for me. I do again find myself in a place where im not comfortable in this 'anxiety brain' being ''published'' in a chatsystem, as i acknowledge that not all, who deal with anxiety, will have the same view on the matter. But as this is a personal task i decided to make a personal emoji, for me it does a fine job of expressing my experience with anxiety but might does so for others. :)

### References

Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. 
