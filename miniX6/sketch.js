let start = 0
let inc = 0.007
let start2 = 0
let inc2 = 0.0075
let sx = 0
let sy = 100
let mx = -50
let my = 150
let mc;
let mc1;
let mc2;
let img;
let img2;
let yoff = 0;
let level1=400;
let level2=440;
let startw = 0;
let mwy = 0;
let myFont;
let button;



function preload(){
  myFont = ('BebasNeue.otf');

}

function setup(){
  let width = 1000,
      height = 400;
createCanvas(width,height);
button = createButton('NETFLIX');
button.style('textFont','myFont');
button.style('font-size', '30px');
button.style("color", "#DF0000");
button.style("background", "#000000");


button.position(width/2-50, height+10);
button.mousePressed(change);
button.mouseOut(revertStyle);

}

function draw() {
    background(my*0.4, my/1.4, my*1.2);

//Conditional statement for the level of the water
    if(level2>200 && startw==1){
      level1 -= 0.2;
      level2 -= 0.2;
  }
// Code for the sun and moon
  noStroke()
  fill(255, 204, 27);
  ellipse(sx,sy,80,80);
  fill('white');
  ellipse(mx,my,80,80)

// From here to line 78 is how the mountains are created
// Line 40-52 is the code for the my first mountin in the back
mc = map(my, 0, 150, 80, 173)
    stroke(0, mc, 218);
    noFill();
    let xoff = start;
  for (let x = 0; x < width; x++){
      let y = noise(xoff)*height*1.5;
        line(x,(y+mwy),x,400)
        xoff += inc;
    }
  start += inc;

// Line 55-77 is the code for the front and middle mountain
    stroke(46,204,246);
    noFill();

    let xoff3 = start
    let xoff2 = start
  for (let x2 = 0; x2 < width; x2++){
      let y2 = noise(xoff2+40)*height*1.7;
      let y21 = noise(xoff3+80)*height/0.55;
    mc1 = map(my, 0, 150, 143, 208)
    mc2 = map(my, 0, 150, 230, 245)
      stroke(46,204,246,230);
      line(x2,(y2+mwy),x2,400)

  push()
    stroke(mc1, mc2, 255,230);
    line(x2,(y21+mwy/1.4),x2,400)
  pop()
    xoff2 += inc2*0.7;
    xoff3 += inc2*0.6
  }

  start2 += inc2;



//Sun movement
  if(sx<=width+49){
    sx++
  }

  if(sx==width+50 && mx == width+50){
    sx=-50
    sy=150
    mx=-50
    sy=150
  }

  if(sx<width/2){
    sy=sy-0.3
  } else if(sx>400){
    sy = sy +0.3
  }

//Moon movement
  if(sx>=width+50){
    mx++
  }

  if(mx<width/2 && sx>=width+50){
    my=my-0.3
  } else if(mx>width/2 && sx>=width+50){
    my = my +0.3
  }
//}


//the complete function for the water - with use of perlin noise()
push()
  fill(100,200,255,200);
  // We are going to draw a polygon out of the wave points
  beginShape();

//same concept as the creation of mountains
  var xwoff = 0; 

  // Setting a for loop, to set up how many connecting dots, which is to be drawn across the canvas 
  for (var xw = 0; xw <= width; xw += 10) {

      // Setting up a code for the Y coordinate for each vertex point -> using perlin noise
      var yw = map(noise(xwoff, yoff), 0, 1, level1, level2);
      vertex(xw, yw);

      //Creating a new perlin noise value for each point, to creating a new coordinate, which make it come to life
      xwoff += 0.06;
  }
  // Something as mentioned above for the x coordinate
  yoff += 0.03;
  vertex(width, height);
  vertex(0, height);
  endShape(CLOSE);
pop()

//setting up how the button change will affect the sketch and how high the water level is allowed to go
 if(level2>200 && startw==1){
   mwy+=0.2
 }

}

function change() {
  button.style("background", "#212121");
  if(startw==0){
    startw=1
  }
}


  function revertStyle() {
    button.style("background", "#000000");
  }

