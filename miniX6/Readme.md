[My Code](https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX6/sketch.js)

[My Project](https://christianrohde.gitlab.io/p5/miniX6/)

<img src ='miniX6.png' width=600>

### My Renewed Project

I really like the outcome of my last miniX (miniX5), so i knew from the beginning of the week that i wanted to further explore what i could do with it. 
I had a bunch of fun ideas, i wanted to incorporate in my sketch, but none of them seem very useful in terms of the theme of the week, critical making. I then began to explore different ways of making something critical, and with inspiration from my buddy group, i decided to work on the obvious (when looking at the mountains/glaciers) and focus on the environmental damaged caused by humans. I chose to focus on how our increased streaming has lead to the harmful implications on our planet. The article 'How do streaming services like Netflix affect our environment?' published by Cambridge Digital Innovation, briefly deals with the claim of 30 minutes of netflix streaming having the same environmental impact as driving 4 miles in a gasoline car. The article does refuse this claim and actually praise Netflix to some extent, but still argues the fact that streaming does cause some damage. 
This was a theme and a case i felt a type of connection to and wanted to visualize. I did this by creating a 'Netflix' button, meant to symbolize the streaming on the platform. The button makes the glaciers melt and the water level rises. I did make an additional button to reset the whole thing, but realized that the message might be more 'intense' to not include a reset button, as we cant reset the state of our planet either.

<br> 

This is also my take on aesthetic programming, as the project is build in code with the purpose of me developing my skills as a programmer, but with a strong conceptual idea behind it, meant to make the user and me as a programmer more aware of the impact our use of digital artifacts and services have on the world around us. This takes us to the connection to Critical Making, as the final product hopefully changes the perception of everything being hosted online, having little to no impact of our environmental issues, as we really cant ''see and grasp it'' compared to the production of DVD's and VHS etc. While it may not be the strongest connection, i still feel a connection between the two, when looking at what Ratto describes in the text “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” (2019);    

<br>

    ''Ultimately, critical making is about turning the relationship between technology and society  from a "matter of fact" into a "matter of concern". I [Ratto] see this as requiring personal investment, a "caring for" that is not typically part of either technical or social scholarly education'.'' (Ratto, p. 20)

<br> 

My intention was to direct the focus of climate change to impact of online streaming, which isnt the most obvious or discussed element of environmental damage, to touch on less published findings.

<br>

#### Programming as practice and the relation to digital culture
Design can be created and explored through unlimited approaches, but programming and more specific Aesthetic programming, allows us think critically upon the consequences, this new era of digital culture has on our society. It makes the whole process of programming more interesting, as it quickly contains a deeper meaning, and forces us to reflect upon every aspect of computation in our lives.

### References and Inspiration
- I took most of the water code from this sketch: https://editor.p5js.org/YiyunJia/sketches/BJz5BpgFm 

- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24

- Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28

-  https://www.cambridgedigitalinnovation.org/single-post/2020/07/08/how-do-streaming-services-like-netflix-affect-our-environment
